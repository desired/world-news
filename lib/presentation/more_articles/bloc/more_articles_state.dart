part of 'more_articles_bloc.dart';

class MoreArticlesState extends Equatable {
  final List<Article>? news;
  final Failure? failure;
  final int len;

  const MoreArticlesState({
    required this.news,
    required this.len,
    this.failure,
  });

  factory MoreArticlesState.initial() {
    return const MoreArticlesState(news: null, len: 0);
  }

  MoreArticlesState copyWith({
    List<Article>? Function()? news,
    Failure? Function()? failure,
    required int len,
  }) {
    return MoreArticlesState(
      len: len,
      news: news != null ? news() : this.news,
      failure: failure != null ? failure() : this.failure,
    );
  }

  bool hasNews() => news != null && (news!.isNotEmpty);

  @override
  List<Object?> get props => [news, failure, len];
}
