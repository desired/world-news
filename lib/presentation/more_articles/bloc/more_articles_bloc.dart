
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:world_news/domain/news/usecases/get_news_by_categorie.dart';
import '../../../core/error/failure.dart';

import '../../../domain/news/entities/article.dart';

part 'more_articles_event.dart';
part 'more_articles_state.dart';

List<Article> art = [];

class MoreArticlesBloc extends Bloc<Equatable, MoreArticlesState> {
  final GetNewsByCategorie getNewsByCategorie;

  MoreArticlesBloc({
    required this.getNewsByCategorie,
  }) : super(MoreArticlesState.initial()) {
    on<MoreArticlesEvent>(_handleMoreEvent);
  }

  void load({required String categorie, int page = 1, bool isNew = true}) =>
      add(MoreArticlesEvent(page: page, categorie: categorie,isNew: isNew));

  void _handleMoreEvent(MoreArticlesEvent event, Emitter emit) async {
    var result = await getNewsByCategorie(
        NewsByCategorieParam(page: event.page, categorie: event.categorie));
        if(event.isNew)  art = [];

    result.fold(
      (l) => emit(state.copyWith(failure: () => l, len: 0)),
      (r) {
        for (var element in r.articles!) {
          art.add(element);
        }
        return emit(state.copyWith(
            news: () => art, len: art.length, failure: () => null));
      },
    );
  }
}
