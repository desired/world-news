part of 'more_articles_bloc.dart';

class MoreArticlesEvent extends Equatable {
  final int page;
  final String categorie;
  final bool isNew;

  const MoreArticlesEvent({required this.page, required this.categorie,required this.isNew});

  @override
  List<Object?> get props => [page, categorie];
}
