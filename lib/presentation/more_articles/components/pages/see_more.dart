import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:world_news/constants.dart';
import 'package:world_news/core/helpers/throttle_helper.dart';

import '../../../../core/helpers/snack_bar_helper.dart';
import '../../../../injector.dart';
import '../../bloc/more_articles_bloc.dart';

class SeeMoreArticles extends StatefulWidget {
  final String categorie;
  const SeeMoreArticles({Key? key, required this.categorie}) : super(key: key);

  @override
  State<SeeMoreArticles> createState() => _SeeMoreArticlesState();
}

class _SeeMoreArticlesState extends State<SeeMoreArticles> {
  int currentPage = 1;
  Throttle throttle = Throttle(
    timeInMS: 300,
    callBack: (value) {
      magic.get<MoreArticlesBloc>().getNewsByCategorie(value!);
    },
  );
  final controller = ScrollController();
  int size = 1;
  @override
  void initState() {
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.offset) {
        setState(() {
          size = currentPage++;
        });
        magic
            .get<MoreArticlesBloc>()
            .load(categorie: widget.categorie, page: size,isNew: false);
        //print('rrrrrrrrrrrrrrrrrrrrrrrrrr$size');
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    throttle.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.categorie.toUpperCase()),
      ),
      body: BlocConsumer<MoreArticlesBloc, MoreArticlesState>(
        bloc: magic(),
        builder: (context, state) {
          if (art.isEmpty) {
            return const Center(child: CircularProgressIndicator());
          }

          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: GridView.builder(
              controller: controller,
              itemBuilder: (BuildContext context, index) {
                //print('art $size ${art[size - 1][0]} ');
                return articleCard(
                    context, state.news![index],
                    );
              },
              itemCount: state.news!.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 250,
                  //childAspectRatio: 3 / 2,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 5),
            ),
          );
        },
        listener: (context, state) {
          if (state.failure == null) return;

          showErrorSnackBack(context, state.failure!.message);
        },
      ),
    );
  }
}
