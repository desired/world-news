import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:world_news/presentation/home/widgets/build_article_list.dart';
import '../../../core/helpers/snack_bar_helper.dart';
import '../blocs/home_bloc.dart';

import '../../../injector.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('World News'),
        centerTitle: true,
      ),
      body: const SafeArea(child: _HomePageListView()),
    );
  }
}

class _HomePageListView extends StatelessWidget {
  const _HomePageListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      bloc: magic(),
      builder: (context, state) {
        if (state.news == null) {
          return const Center(child: CircularProgressIndicator());
        }

        return buildArticleList(news: state.news!);
      },
      listener: (context, state) {
        if (state.failure == null) return;

        showErrorSnackBack(context, state.failure!.message);
      },
    );
  }
}
