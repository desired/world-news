import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:world_news/constants.dart';
import 'package:world_news/domain/news/entities/news.dart';
import 'package:world_news/presentation/more_articles/bloc/more_articles_bloc.dart';

import '../../../injector.dart';

Widget buildArticleList({
  required List<News> news,
}) {
  return ListView.builder(
    itemCount: news.length,
    padding: const EdgeInsets.only(left: 10),
    itemBuilder: (BuildContext context, int i) => Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                news[i].categorie.toString().toUpperCase(),
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              TextButton(
                onPressed: () {
                  /// Start initiliaze home bloc
                  magic
                      .get<MoreArticlesBloc>()
                      .load(categorie: news[i].categorie.toString());
                  context.goNamed(
                    'seemore',
                    extra: news[i].categorie.toString(),
                  );
                },
                child: const Text(
                  'Voir plus',
                  style: TextStyle(color: Colors.green),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.4,
          height: MediaQuery.of(context).size.width * 0.45,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: news[i].articles!.length,
              itemBuilder: (BuildContext context, int j) {
                return articleCard(context, news[i].articles![j]);
              }),
        ),
      ],
    ),
    scrollDirection: Axis.vertical,
  );
}
