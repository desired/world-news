part of 'home_bloc.dart';

class HomeState extends Equatable {
  final List<News>? news;
  final Failure? failure;

  const HomeState({required this.news, this.failure});

  factory HomeState.initial() {
    return const HomeState(news: null);
  }

  HomeState copyWith(
      {List<News>? Function()? news, Failure? Function()? failure}) {
    return HomeState(
      news: news != null ? news() : this.news,
      failure: failure != null ? failure() : this.failure,
    );
  }

  @override
  List<Object?> get props => [news, failure];
}
