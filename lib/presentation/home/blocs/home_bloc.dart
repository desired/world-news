import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/error/failure.dart';
import '../../../core/usecases/usecase.dart';

import '../../../domain/news/entities/news.dart';
import '../../../domain/news/usecases/get_top_news.dart';

part 'home_state.dart';
part 'home_events.dart';

class HomeBloc extends Bloc<Equatable, HomeState> {
  final GetTopNews getTopNews;

  HomeBloc({
    required this.getTopNews,
  }) : super(HomeState.initial()) {
    on<HomeLoadEvent>(_handleLoadEvent);
  }

  void load() => add(HomeLoadEvent());

  void _handleLoadEvent(HomeLoadEvent event, Emitter emit) async {
    var result = await getTopNews(NoParams());

    result.fold(
      (l) => emit(state.copyWith(failure: () => l)),
      (r) {
        return emit(state.copyWith(news: () => r, failure: () => null));
      },
    );
  }
}
