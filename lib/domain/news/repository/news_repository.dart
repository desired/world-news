import '../../../core/error/failure.dart';
import 'package:dartz/dartz.dart';
import '../../../data/news/datasources/news_datasource.dart';

import '../entities/news.dart';

/// This class cast `NewsDataSource` return value
/// from `model NewsModel` to `entitie News`
/// Which allows us to go from data layer to domain layer safely.
class NewsRepository {
  final NewsDataSource newsDataSource;

  NewsRepository({required this.newsDataSource});

  Future<Either<Failure, List<News>>> getTopHeadlines() async {
    return await newsDataSource.getHeadline(NewsDataType.topHeadlines);
  }

  Future<Either<Failure, News>> getNewsByCategorie(
      {required int page, required String categorie}) async {
    return await newsDataSource.getNewsByCategorie(
        NewsDataType.getNewsByCategorie,
        arg: page,
        arg2: categorie);
  }
}
