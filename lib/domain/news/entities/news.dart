import 'package:equatable/equatable.dart';

import 'article.dart';

class News extends Equatable {
  final String? status;
   final String? categorie;
  final int? totalResults;
  final List<Article>? articles;

   const News(
      {required this.categorie,
      required this.status,
      required this.totalResults,
      required this.articles});

  set articlesSet(List<Article> newArticles) {
    articles!.addAll(newArticles);
  }


  @override
  List<Object?> get props => [categorie, status, totalResults, articles];
}
