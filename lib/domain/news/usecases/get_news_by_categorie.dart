import 'package:dartz/dartz.dart';
import 'package:world_news/core/usecases/usecase.dart';
import 'package:world_news/domain/news/entities/news.dart';
import 'package:world_news/domain/news/repository/news_repository.dart';

import '../../../core/error/failure.dart';

class NewsByCategorieParam {
  final int page;
  final String categorie;

  const NewsByCategorieParam({required this.page, required this.categorie});
}

class GetNewsByCategorie extends UseCase<News, NewsByCategorieParam> {
  final NewsRepository newsRepository;

  const GetNewsByCategorie(this.newsRepository);

  @override
  Future<Either<Failure, News>> call(NewsByCategorieParam params) async {
    var result = await newsRepository.getNewsByCategorie(
        page: params.page, categorie: params.categorie);

    return result;
  }
}
