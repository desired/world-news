import 'package:flutter/material.dart';

import 'domain/news/entities/article.dart';

const String notFoundImageUrl =
    'https://www.adenine-rh.com/wp-content/themes/consultix/images/no-image-found-360x250.png';

const String newsAPIapiKey = '3b648ad42d4b4859bd351ac7988bcfb3';

const List categories = [
  'business',
  'entertainment',
  'general',
  'health',
  'science',
  'sports',
  'technology',
];
Column articleCard(
  BuildContext context,
  Article article,
) {
  return Column(
    children: [
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width * 0.4,
        height: MediaQuery.of(context).size.width * 0.4,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(article.urlToImage ?? notFoundImageUrl),
            fit: BoxFit.cover,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(20),
          ),
          color: Colors.amber[100],
        ),
      ),
      SizedBox(
        width: MediaQuery.of(context).size.width * 0.4,
        height: MediaQuery.of(context).size.width * 0.05,
        child: Text(
          article.title ?? 'title not found',
          overflow: TextOverflow.ellipsis,
        ),
      ),
    ],
  );
}
