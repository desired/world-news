import '../../../domain/news/entities/news.dart';
import 'article_model.dart';

class NewsModel extends News {
  @override
  // ignore: overridden_fields
  final List<ArticleModel>? articles;
  // ignore: overridden_fields, annotate_overrides
  String? categori = 'musique';

  NewsModel({String? status, int? totalResults, this.articles, this.categori})
      : super(
          status: status,
          totalResults: totalResults,
          articles: articles,
          categorie: categori,
        );

  factory NewsModel.fromJson(Map<String, dynamic> json) {
    List<ArticleModel>? articles;

    if (json['articles'] != null) {
      articles = <ArticleModel>[];
      json['articles'].forEach((v) {
        articles!.add(ArticleModel.fromJson(v));
      });
    }

    return NewsModel(
      status: json['status'],
      totalResults: json['totalResults'],
      articles: articles,
      categori: json['categorie'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['totalResults'] = totalResults;
    data['categorie'] = categori;
    if (articles != null) {
      data['articles'] = articles!.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  List<Object?> get props => [categori];
  @override
  String? get categorie => categori;
  // set categorie(String? categorie) => categori;
}
