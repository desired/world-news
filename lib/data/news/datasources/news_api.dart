/* 
# dart
pub run build_runner build

# flutter	
flutter pub run build_runner build
*/

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../models/news_model.dart';

part 'news_api.g.dart';

@RestApi(
    baseUrl:
        "https://newsapi.org/v2//everything?from=2022-04-8&sortBy=publishedAt&pageSize=10")
abstract class NewsApi {
  factory NewsApi(Dio dio, {String baseUrl}) = _NewsApi;

  @GET("&page=1?country={country}&q={categorie}")
  Future<NewsModel> getTopHeadlines(
      {@Path() String country = 'fr', @Path() required String categorie});
  @GET('&page={page}?country={country}&q={categorie}')
  Future<NewsModel> getNewsByCategorie(
      {@Path() String country = 'fr',
      @Path() required String categorie,
      @Path() required int page});
}
