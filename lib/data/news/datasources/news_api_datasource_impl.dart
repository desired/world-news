import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:world_news/constants.dart';

import '../../../core/error/failure.dart';
import '../../../core/interceptors/dio_error_interceptor.dart';
import '../../../core/interceptors/dio_request_interceptor.dart';
import '../models/news_model.dart';
import 'news_api.dart';
import 'news_datasource.dart';

/// Implementation for https://newsapi.org/
class NewsApiDataSourceImpl implements NewsDataSource {
  /// Dio instance
  /// @RequestInterceptor allows us to inject API key for each request we make
  /// @ErrorInterceptor allows us to transform raw exceptions into managed failures
  final Dio dio = Dio()
    ..interceptors.add(RequestInterceptor())
    ..interceptors.add(ErrorInterceptor());

  late NewsApi _newsApi;

  NewsApiDataSourceImpl() {
    _newsApi = NewsApi(dio);
  }

  Future<Either<Failure, List<NewsModel>>> _getTopHeadlines() async {
    try {
      List<NewsModel> temp = [];
      for (var i = 0; i < categories.length; i++) {
        NewsModel result =
            await _newsApi.getTopHeadlines(categorie: categories[i]);
        result.categori = categories[i];
        temp.add(result);
      }
      // print('temp $temp');
      List<NewsModel> result = temp;
      return right(result);
    } catch (ex) {
      return Left(ex is DioError ? ex.error : const UnknownFailure());
    }
  }

  Future<Either<Failure, NewsModel>> _getNewsByCategorie(
      {required int page, required String categorie}) async {
    try {
      var result = await _newsApi.getNewsByCategorie(
        categorie: categorie,
        page: page,
      );

      return right(result);
    } catch (ex) {
      return Left(ex is DioError ? ex.error : const UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, List<NewsModel>>> getHeadline(NewsDataType type,
      {Object? arg}) async {
    return _getTopHeadlines();
    // }
  }

  @override
  Future<Either<Failure, NewsModel>> getNewsByCategorie(NewsDataType type,
      {required Object arg, required Object arg2}) async {
    return _getNewsByCategorie(page: arg as int, categorie: arg2 as String);
  }
}
