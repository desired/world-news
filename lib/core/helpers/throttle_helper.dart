import 'dart:async';

///
/// Class permettant limiter l'exécution des fonctions au dela
/// d'un certain temps donné (en milliseconds)
///
class Throttle<T> {
  Timer? _timer;
  Function callBack;

  final int timeInMS;

  Throttle({
    required this.timeInMS,
    required this.callBack,
  });

  void cancel() => _timer?.cancel();

  /// Rappeler la fonction passée en [callBack] de l'objet Throttle
  void restart(T? param) {
    if (_timer != null) {
      _timer?.cancel();
    }

    _timer = Timer(Duration(milliseconds: timeInMS), () {
      callBack(param);
    });
  }
}
