import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'injector.dart';
import 'presentation/home/blocs/home_bloc.dart';
import 'presentation/home/pages/home_page.dart';
import 'presentation/more_articles/components/pages/see_more.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  initializeInjector();

  /// Start initiliaze home bloc
  magic.get<HomeBloc>().load();

  runApp(const MyApp());
}

final _router = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
      path: '/',
      name: 'home',
      // builder: (context, state) => const CategoriesPage(),
      builder: (context, state) => const HomePage(),
      routes: [
        GoRoute(
          path: 'seemore',
          name: 'seemore',
          builder: (context, state) {
            return SeeMoreArticles(
              categorie: state.extra as String,
            ); //DetailsPage(article: state.extra as Article);
          },
        ),
      ],
    ),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(primarySwatch: Colors.green),
      routerDelegate: _router.routerDelegate,
      routeInformationParser: _router.routeInformationParser,
    );
  }
}
